// Homework19.cpp : This file contains the 'main' function. Program execution begins and ends there.
//

#include <iostream>
class Animal
{
public:
    virtual void Voice()
    {
        std::cout << "AAAAA!" << '\n';
    }
};
class Dog : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Dog goes woof" << '\n';
    }
};
class Cat : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Cat goes meow" << '\n';
    }
};
class Bird : public Animal
{
public:
    void Voice() override
    {
        std::cout << "Bird goes tweet" << '\n';
    }
};
class Mouse : public Animal
{
public:
    void Voice() override
    {
        std::cout << "And mouse goes squeek" << '\n';
    }
};
int main()
{
    Animal *animals[4];
    animals[0] = new Dog;
    animals[1] = new Cat;
    animals[2] = new Bird;
    animals[3] = new Mouse;
    for (int i = 0; i < 4; i++)
    {
        animals[i]->Voice();
    }

}


